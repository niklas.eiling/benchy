///! This is a module description
///! Author: Niklas Eiling
const std = @import("std");
const print = std.debug.print;
const mem = std.mem;
const ChildProcess = std.ChildProcess;

const expect = std.testing.expect;

test "call" {
    try call(std.testing.allocator);
}

pub fn call(alloc: *mem.Allocator) !void {
    const cmd = [_][]const u8{ "cuobjdump", "--dump-sass", "/opt/cuda/samples/0_Simple/matrixMul/matrixMul" };
    const result = try std.ChildProcess.exec(.{
        .allocator = alloc,
        .argv = &cmd,
        .max_output_bytes = 1024 * 1024 * 1024,
    });
    defer alloc.free(result.stdout);
    errdefer alloc.free(result.stderr);
    print("output: {s}\n", .{result.stdout});
    print("child returned {}\n", .{result.term.Exited});
    if (result.term.Exited != 0)
        return error.ChildFailed;
    alloc.free(result.stderr);
}
