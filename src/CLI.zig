///! This is a module description
///! Author: Niklas Eiling
const std = @import("std");
const print = std.debug.print;
const process = std.process;
const mem = std.mem;

const expect = std.testing.expect;

pub fn Arguments(comptime arg_t: type, comptime optkey_t: type, comptime flag_t: type) !type {
    if (@typeInfo(arg_t) != .Enum or @typeInfo(optkey_t) != .Enum or @typeInfo(flag_t) != .Enum) {
        return error.NotAnEnum;
    }
    const opt_t = struct {
        key: optkey_t,
        val: [:0]const u8,
    };
    return struct {
        cmd: [:0]const u8,
        arguments: []arg_t,
        arguments_help: ?[@typeInfo(arg_t).Enum.fields.len][:0]const u8,
        options: []opt_t,
        options_help: ?[@typeInfo(optkey_t).Enum.fields.len][:0]const u8,
        flags: []flag_t,
        flags_help: ?[@typeInfo(flag_t).Enum.fields.len][:0]const u8,
        allocator: mem.Allocator,
        pub fn init(alloc: mem.Allocator, ap: *ArgumentsParser) !@This() {
            var ret = @This(){
                .cmd = ap.cmd,
                .arguments = try alloc.alloc(arg_t, ap.args.items.len),
                .options = try alloc.alloc(opt_t, ap.options.items.len),
                .flags = try alloc.alloc(flag_t, ap.flags.items.len),
                .arguments_help = null,
                .options_help = null,
                .flags_help = null,
                .allocator = alloc,
            };
            var i: usize = 0;
            for (ap.args.items) |item| {
                if (!inline for (@typeInfo(arg_t).Enum.fields) |f| {
                    if (mem.eql(u8, f.name, item)) {
                        ret.arguments[i] = @intToEnum(arg_t, f.value);
                        i += 1;
                        break true;
                    }
                } else false) {
                    std.log.warn("{s} is not a valid argument.", .{item});
                }
            }
            ret.arguments = ret.arguments[0..i];

            i = 0;
            for (ap.options.items) |item| {
                if (!inline for (@typeInfo(optkey_t).Enum.fields) |f| {
                    if (mem.eql(u8, f.name, item.key)) {
                        ret.options[i].key = @intToEnum(optkey_t, f.value);
                        ret.options[i].val = item.val;
                        i += 1;
                        break true;
                    }
                } else false) {
                    std.log.warn("{s} is not a valid option.", .{item.key});
                }
            }
            ret.options = ret.options[0..i];

            i = 0;
            for (ap.flags.items) |item| {
                if (!inline for (@typeInfo(flag_t).Enum.fields) |f| {
                    if (item == f.name[0]) {
                        ret.flags[i] = @intToEnum(flag_t, f.value);
                        i += 1;
                        break true;
                    }
                } else false) {
                    std.log.warn("{} is not a valid flag.", .{item});
                }
            }
            ret.flags = ret.flags[0..i];
            return ret;
        }
        pub fn hasArg(self: @This(), arg: arg_t) bool {
            for (self.arguments) |item| {
                if (item == arg) {
                    return true;
                }
            }
            return false;
        }
        pub fn getOpt(self: @This(), opt: optkey_t) ?[:0]const u8 {
            for (self.options) |item| {
                if (item.key == opt) {
                    return item.val;
                }
            }
            return null;
        }
        pub fn hasFlag(self: @This(), flag: flag_t) bool {
            for (self.flags) |item| {
                if (item == flag) {
                    return true;
                }
            }
            return false;
        }
        pub fn getArgs(alloc: mem.Allocator) !@This() {
            const argStrings = try getArgStrings(alloc);
            var argParser = ArgumentsParser.init(alloc);
            defer argParser.deinit();
            try argParser.parseStrings(argStrings.items);
            var CLIArgs = try init(alloc, &argParser);
            return CLIArgs;
        }
        pub fn setArgumentsHelp(self: *@This(), help: [@typeInfo(arg_t).Enum.fields.len][:0]const u8) !void {
            self.arguments_help = help;
        }
        pub fn setOptionsHelp(self: *@This(), help: [@typeInfo(optkey_t).Enum.fields.len][:0]const u8) !void {
            self.options_help = help;
        }
        pub fn setFlagsHelp(self: *@This(), help: [@typeInfo(flag_t).Enum.fields.len][:0]const u8) !void {
            self.flags_help = help;
        }

        fn printHelpFromEnum(comptime enumType: type, prefixStr: [:0]const u8, helpStrs: ?[@typeInfo(enumType).Enum.fields.len][:0]const u8) void {
            if (helpStrs) |help| {
                inline for (@typeInfo(enumType).Enum.fields) |f| {
                    print("\t{s}{s}: {s}\n", .{ prefixStr, f.name, help[f.value] });
                }
            } else {
                inline for (@typeInfo(enumType).Enum.fields) |f, i| {
                    print("{s}{s}", .{ prefixStr, f.name });
                    if (i < @typeInfo(enumType).Enum.fields.len - 1) {
                        print(", ", .{});
                    }
                }
                print("\n", .{});
            }
        }

        pub fn printHelp(self: @This()) void {
            print("Help for {s}\n", .{self.cmd});
            print("\n", .{});
            print("Possible Arguments:\n", .{});
            printHelpFromEnum(arg_t, "", self.arguments_help);
            print("Possible Options:\n", .{});
            printHelpFromEnum(optkey_t, "--", self.options_help);
            print("Possible Flags:\n", .{});
            printHelpFromEnum(flag_t, "-", self.flags_help);
        }

        pub fn deinit(self: @This()) void {
            self.allocator.free(self.arguments);
            self.allocator.free(self.options);
            self.allocator.free(self.flags);
        }
    };
}

test "Arguments" {
    const args = enum { arg1, arg2 };
    const opts = enum { opt1, opt2 };
    const flags = enum { f, l, a, g };
    const strings = [_][:0]const u8{ "cmd", "argument1", "arg1", "-fl", "--option1", "value1", "--opt2", "value2", "-ag", "arg2" };
    var argParser = ArgumentsParser.init(std.testing.allocator);
    try argParser.parseStrings(strings[0..]);
    const CLIType = try Arguments(args, opts, flags);
    var CLIArgs = try CLIType.init(std.testing.allocator, &argParser);
    argParser.deinit();
    try expect(mem.eql(u8, CLIArgs.cmd, "cmd"));
    try expect(CLIArgs.arguments.len == 2);
    try expect(CLIArgs.options.len == 1);
    try expect(CLIArgs.flags.len == 4);
    try expect(CLIArgs.arguments[0] == args.arg1);
    try expect(CLIArgs.arguments[1] == args.arg2);
    try expect(CLIArgs.options[0].key == opts.opt2);
    try expect(mem.eql(u8, CLIArgs.options[0].val, "value2"));
    try expect(CLIArgs.flags[0] == flags.f);
    try expect(CLIArgs.flags[1] == flags.l);
    try expect(CLIArgs.flags[2] == flags.a);
    try expect(CLIArgs.flags[3] == flags.g);
    CLIArgs.deinit();
    //try expect(x == true);
}

pub fn getArgStrings(allocator: mem.Allocator) !std.ArrayList([:0]u8) {
    var al = std.ArrayList([:0]u8).init(allocator);
    var arg_it = process.args();
    while (arg_it.next(allocator)) |errarg| {
        const arg = try errarg;
        try al.append(arg);
    }
    return al;
}

const KeyVal = struct {
    key: [:0]const u8,
    val: [:0]const u8,
};

pub const ArgumentsParser = struct {
    cmd: [:0]const u8,
    args: std.ArrayList([:0]const u8),
    options: std.ArrayList(KeyVal),
    flags: std.ArrayList(u8),

    pub fn init(allocator: mem.Allocator) ArgumentsParser {
        return ArgumentsParser{
            .cmd = "",
            .args = std.ArrayList([:0]const u8).init(allocator),
            .options = std.ArrayList(KeyVal).init(allocator),
            .flags = std.ArrayList(u8).init(allocator),
        };
    }
    pub fn parseStrings(self: *ArgumentsParser, argStrings: []const [:0]const u8) !void {
        var i: usize = 1;
        if (argStrings.len == 0) {
            return error.InvalidArgs;
        }
        var arg: [:0]const u8 = argStrings[0];
        self.cmd = arg;
        while (argStrings.len > i) {
            arg = argStrings[i];
            if (mem.startsWith(u8, arg, "--")) {
                if (argStrings.len > i + 1) {
                    try self.options.append(.{ .key = arg[2..], .val = argStrings[i + 1] });
                    i += 1;
                } else {
                    return error.InvalidArgs;
                }
            } else if (mem.startsWith(u8, arg, "-")) {
                try self.flags.appendSlice(arg[1..]);
            } else {
                try self.args.append(arg);
            }
            i += 1;
        }
    }
    pub fn output(self: ArgumentsParser) void {
        std.log.debug("cmd: {s}\n", .{self.cmd});
        std.log.debug("options:\n", .{});
        for (self.options.items) |o| {
            std.log.debug("\t{s} = {s}\n", .{ o.key, o.val });
        }
        std.log.debug("flags: ", .{});
        for (self.flags.items) |f| {
            std.log.debug("{c}, ", .{f});
        }
        std.log.debug("\n", .{});
        std.log.debug("args:\n", .{});
        for (self.args.items) |a| {
            std.log.debug("\t{s}\n", .{a});
        }
    }
    pub fn deinit(self: ArgumentsParser) void {
        self.args.deinit();
        self.options.deinit();
        self.flags.deinit();
    }
};
test "ArgumentsParser" {
    const strings = [_][:0]const u8{ "cmd", "argument1", "-fl", "--option1", "value1", "--option2", "value2", "-ag", "argument2" };
    var args = ArgumentsParser.init(std.testing.allocator);
    try args.parseStrings(strings[0..]);
    try expect(mem.eql(u8, args.cmd, "cmd"));
    try expect(args.args.items.len == 2);
    try expect(args.options.items.len == 2);
    try expect(args.flags.items.len == 4);
    try expect(mem.eql(u8, args.args.items[0], "argument1"));
    try expect(mem.eql(u8, args.args.items[1], "argument2"));
    try expect(mem.eql(u8, args.options.items[0].key, "option1"));
    try expect(mem.eql(u8, args.options.items[0].val, "value1"));
    try expect(mem.eql(u8, args.options.items[1].key, "option2"));
    try expect(mem.eql(u8, args.options.items[1].val, "value2"));
    try expect(args.flags.items[0] == 'f');
    try expect(args.flags.items[1] == 'l');
    try expect(args.flags.items[2] == 'a');
    try expect(args.flags.items[3] == 'g');
    args.deinit();
}
