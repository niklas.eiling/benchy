///! This is a module description
///! Author: Niklas Eiling
const std = @import("std");
const print = std.debug.print;
const mem = std.mem;
const json = std.json;

const expect = std.testing.expect;
const CLI = @import("CLI.zig");
const process = @import("process.zig");

pub fn isEnum(comptime obj: type) !void {
    if (@typeInfo(obj) != .Enum) {
        return error.NotAnEnum;
    }
    std.log.warn("enum fields: {}\n", .{@typeInfo(obj).Enum.fields.len});
}

test "Enum test" {
    const args = enum {
        help,
        iterations,
        verbose,
    };
    //var arg = args.help;
    try isEnum(args);
    try std.testing.expect(if (isEnum(u8)) false else |err| err == error.NotAnEnum);
}

pub fn splitToArray(alloc: mem.Allocator, cmd: [:0]const u8) ![][]const u8 {
    var arr = try alloc.alloc([]const u8, std.mem.count(u8, cmd, " ") + 1);
    var iter = std.mem.split(u8, cmd, " ");
    var i: usize = 0;
    while (iter.next()) |item| {
        arr[i] = item;
        i += 1;
    }
    return arr;
}

pub fn printResult(a: mem.Allocator, res: process.resultTimes) !void {
    const avg = try process.fmtTime(a, res.avg);
    defer a.free(avg);
    const dev = try process.fmtTime(a, res.stdDev);
    defer a.free(dev);
    print("avg: {s}, stdDev: {s}\n", .{ avg, dev });
    if (res.internalAvg) |it| {
        const avgI = try process.fmtTime(a, it);
        defer a.free(avgI);
        print("avgI: {s}, ", .{avgI});
    }
    if (res.internalStdDev) |it| {
        const devI = try process.fmtTime(a, it);
        defer a.free(devI);
        print("stdDevI: {s}", .{devI});
    }
    print("\n", .{});
    if (res.internalFloatAvg) |it| {
        print("avgF: {}, ", .{it});
    }
    if (res.internalFloatStdDev) |it| {
        print("stdDevF: {}", .{it});
    }
    print("\n", .{});
}

/// Entry point to this program
pub fn main() !void {
    std.log.info("Hello World.", .{});
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const a = arena.allocator();

    const args = enum { run, help, redo };
    const args_help = std.enums.directEnumArray(args, [:0]const u8, 0, .{
        .run = "run an executable measuring its execution time.",
        .help = "show this help.",
        .redo = "rerun a previous measurment comparing it to the previous.",
    });
    const opts = enum { cmd, iterations, timerfmt, floatfmt, warmup, env, out};
    const opts_help = std.enums.directEnumArray(opts, [:0]const u8, 0, .{
        .cmd = "the command to run.",
        .iterations = "the number of iterations to average over.",
        .timerfmt = "format string for timer outputs.",
        .floatfmt = "format string for float outputs.",
        .warmup = "the number of warmup interations to perform before measurements.",
        .env = "set environment variables for runs.",
        .out = "output file for results.",
    });
    const flags = enum { a, d, i, h, q };
    const flags_help = std.enums.directEnumArray(flags, [:0]const u8, 0, .{
        .a = "",
        .d = "",
        .i = "",
        .h = "show this help.",
        .q = "be quiet."
    });
    var CLIArgs = try (try CLI.Arguments(args, opts, flags)).getArgs(a);
    defer CLIArgs.deinit();
    try CLIArgs.setArgumentsHelp(args_help);
    try CLIArgs.setOptionsHelp(opts_help);
    try CLIArgs.setFlagsHelp(flags_help);
    if (CLIArgs.hasArg(args.help) or
        CLIArgs.hasFlag(flags.h))
    {
        CLIArgs.printHelp();
        return;
    }
    if (CLIArgs.hasArg(args.run)) {
        var progress = std.Progress{};
        var node = try progress.start("", 0);
        defer node.end();
        const cmd = CLIArgs.getOpt(opts.cmd);
        if (cmd == null) {
            CLIArgs.printHelp();
            return;
        }
        var env = if (CLIArgs.getOpt(opts.env)) |e|
            try splitToArray(a, e)
        else
            null;
        if (CLIArgs.getOpt(opts.out)) |out| {
            const file = try std.fs.cwd().openFile(out, .{ .write = true});
            defer file.close();
        }
        const config = process.callConfig{
            .cmd = try splitToArray(a, cmd.?),
            .iterations = try std.fmt.parseUnsigned(u32, CLIArgs.getOpt(opts.iterations) orelse "10", 10),
            .warmUpIterations = try std.fmt.parseUnsigned(u32, CLIArgs.getOpt(opts.warmup) orelse "0", 10),
            .internalTimerFmt = CLIArgs.getOpt(opts.timerfmt) orelse null,
            .floatFmt = CLIArgs.getOpt(opts.floatfmt) orelse null,
            .quite = CLIArgs.hasFlag(flags.q),
            .individualTimes = CLIArgs.hasFlag(flags.i),
            .env = env,
        };
        try json.stringify(config, json.StringifyOptions{ }, std.io.getStdOut().writer());
        node.completeOne();
        progress.refresh();
        const res = try process.callLoop(a, config, node, null);
        try printResult(a, res);
    }
}
