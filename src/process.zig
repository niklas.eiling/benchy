///! This is a module description
///! Author: Niklas Eiling
const std = @import("std");
const print = std.debug.print;
const mem = std.mem;
const ChildProcess = std.ChildProcess;

const expect = std.testing.expect;

pub const resultTimes = struct {
    avg: u64,
    stdDev: u64,
    internalAvg: ?u64,
    internalStdDev: ?u64,
    internalFloatAvg: ?f64,
    internalFloatStdDev: ?f64,
    iterations: usize,
};

const callResult = struct {
    exec: ChildProcess.ExecResult,
    time: u64,

    pub fn free(self: @This(), alloc: mem.Allocator) void {
        alloc.free(self.exec.stdout);
        alloc.free(self.exec.stderr);
    }
};

pub const callConfig = struct {
    cmd: []const []const u8,
    warmUpIterations: u32 = 0,
    iterations: u32,
    internalTimerFmt: ?[:0]const u8 = null,
    floatFmt: ?[:0]const u8 = null,
    quite: bool = false,
    individualTimes: bool = false,
    env: ?[]const []const u8 = null,
};

pub fn floatParse(output: []const u8, floatFmt: [:0]const u8) !f64 {
    var iter = std.mem.split(u8, output, "\n");
    var parsedNum: f64 = 0.0;
    while (iter.next()) |line| {
        const lineTrimmed = std.mem.trim(u8, line, &std.ascii.spaces);
        var iFmt: usize = 0;
        var iLine: usize = 0;
        while (iLine < lineTrimmed.len and
            iFmt < floatFmt.len)
        {
            if (floatFmt[iFmt] == '{') {
                if (iFmt + 2 >= floatFmt.len) {
                    return error.InvalidTimerFmt;
                }
                iFmt += 1;
                const iLineStart = iLine;
                if (floatFmt[iFmt] == 'f') {
                    while (iLine < lineTrimmed.len and
                        (std.ascii.isDigit(lineTrimmed[iLine]) or
                        lineTrimmed[iLine] == '.'))
                    {
                        iLine += 1;
                    }
                    if (iLineStart == iLine) {
                        break;
                    }
                    parsedNum = try std.fmt.parseFloat(f64, lineTrimmed[iLineStart..iLine]);
                }
                iFmt += 1;
                if (floatFmt[iFmt] != '}') {
                    return error.InvalidTimerFmt;
                }
            } else if (lineTrimmed[iLine] != floatFmt[iFmt]) {
                break;
            } else {
                iLine += 1;
            }
            iFmt += 1;
        }
        if (iFmt == floatFmt.len) {
            //matched
            return parsedNum;
        }
    }
    return error.ParseNotFound;
}

pub fn internalTimerParse(output: []const u8, internalTimerFmt: [:0]const u8) !u64 {
    var iter = std.mem.split(u8, output, "\n");
    var time_s: ?u64 = null;
    var time_m: ?u64 = null;
    var time_u: ?u64 = null;
    var time_n: ?u64 = null;
    while (iter.next()) |line| {
        const lineTrimmed = std.mem.trim(u8, line, &std.ascii.spaces);
        var iFmt: usize = 0;
        var iLine: usize = 0;
        while (iLine < lineTrimmed.len and
            iFmt < internalTimerFmt.len)
        {
            if (internalTimerFmt[iFmt] == '{') {
                if (iFmt + 2 >= internalTimerFmt.len) {
                    return error.InvalidTimerFmt;
                }
                iFmt += 1;
                const iLineStart = iLine;
                while (iLine < lineTrimmed.len and
                    std.ascii.isDigit(lineTrimmed[iLine]))
                {
                    iLine += 1;
                }
                if (iLineStart == iLine) {
                    break;
                }
                const parsedNum = try std.fmt.parseUnsigned(u64, lineTrimmed[iLineStart..iLine], 10);
                switch (internalTimerFmt[iFmt]) {
                    's' => {
                        time_s = parsedNum;
                    },
                    'm' => {
                        time_m = parsedNum;
                    },
                    'u' => {
                        time_u = parsedNum;
                    },
                    'n' => {
                        time_n = parsedNum;
                    },
                    else => {
                        return error.InvalidTimerFmt;
                    },
                }
                iFmt += 1;
                if (internalTimerFmt[iFmt] != '}') {
                    return error.InvalidTimerFmt;
                }
            } else if (lineTrimmed[iLine] != internalTimerFmt[iFmt]) {
                break;
            } else {
                iLine += 1;
            }
            iFmt += 1;
        }
        if (iFmt == internalTimerFmt.len) {
            //matched
            return (time_s orelse 0) * std.time.ns_per_s +
                (time_m orelse 0) * std.time.ns_per_ms +
                (time_u orelse 0) * std.time.ns_per_us +
                (time_n orelse 0);
        }
    }
    return error.ParseNotFound;
}

pub fn call(alloc: mem.Allocator, cmd: []const []const u8, env: ?[]const []const u8, timer_arg: ?*std.time.Timer) !callResult {
    const timer = timer_arg orelse &(try std.time.Timer.start());
    var envmap = std.BufMap.init(alloc);
    defer envmap.deinit();
    if (env) |e| {
        for (e) |envvar| {
            var splitEnvvar = std.mem.split(u8, envvar, "=");
            try envmap.put(splitEnvvar.next().?, splitEnvvar.next() orelse "");
            if (splitEnvvar.next() != null) {
                std.log.err("too many \"=\" in env variable", .{});
                return error.InputError;
            }
        }
        //try std.ChildProcess.createNullDelimitedEnvMap(alloc, &envmap);
    }
    timer.reset();
    const result = try std.ChildProcess.exec(.{
        .allocator = alloc,
        .argv = cmd,
        .max_output_bytes = 1024 * 1024,
        .env_map = &envmap,
    });
    const time = timer.read();
    //    if (result.term.Exited != 0)
    //        return error.ChildFailed;
    return callResult{
        .exec = result,
        .time = time,
    };
}

pub fn average(vals: []const u64) u64 {
    var sum: u64 = 0;
    for (vals) |v| {
        sum += v;
    }
    return sum / vals.len;
}

pub fn averageFloat(vals: []const f64) f64 {
    var sum: f64 = 0.0;
    for (vals) |v| {
        sum += v;
    }
    return sum / @intToFloat(f64, vals.len);
}

pub fn stdDevFloat(vals: []const f64) f64 {
    const avg = averageFloat(vals);
    var sum: f64 = 0.0;
    for (vals) |v| {
        sum += (v - avg) * (v - avg);
    }
    return std.math.sqrt(sum / @intToFloat(f64, vals.len));
}

pub fn stdDev(vals: []const u64) u64 {
    const avg = average(vals);
    var sum: u64 = 0;
    for (vals) |v| {
        if (v > avg) {
            sum += (v - avg) * (v - avg);
        } else {
            sum += (avg - v) * (avg - v);
        }
    }
    return std.math.sqrt(sum / vals.len);
}

pub fn processPrint(config: callConfig, out: ?std.fs.File, comptime fmt: [:0]const u8, args: anytype) void {
    if (config.quite == false) {
        print(fmt, args);
    }
    if (out) |o| {
        if (o.writer().print(fmt, args)) {} else |err| {
            std.log.err("error writing to output: {}", .{err});
        }
    }
}

pub fn warmup(alloc: mem.Allocator, config: callConfig, progress: ?*std.Progress.Node) !void {
    if (config.warmUpIterations == 0) return;
    var node: ?std.Progress.Node = undefined;
    if (progress != null) {
        node = progress.?.start("warming up", config.warmUpIterations);
        defer node.?.end();
        node.?.activate();
    } else {
        node = null;
    }
    var warmupI: usize = 0;
    while (warmupI < config.warmUpIterations) {
        if (node != null) node.?.completeOne();
        _ = try call(alloc, config.cmd, config.env, null);
        warmupI += 1;
    }
}

pub fn callLoop(alloc: mem.Allocator, config: callConfig, progress: ?*std.Progress.Node, out: ?std.fs.File) !resultTimes {
    var node: ?std.Progress.Node = undefined;
    if (progress != null) {
        node = progress.?.start("running", config.iterations);
        defer node.?.end();
    } else {
        node = null;
    }

    var timer = try std.time.Timer.start();
    var times = try alloc.alloc(u64, config.iterations);
    defer alloc.free(times);
    var internalTimes: ?[]u64 = null;
    var internalFloats: ?[]f64 = null;
    if (config.internalTimerFmt) |_| {
        internalTimes = try alloc.alloc(u64, config.iterations);
        defer alloc.free(internalTimes.?);
    }
    if (config.floatFmt) |_| {
        internalFloats = try alloc.alloc(f64, config.iterations);
        defer alloc.free(internalFloats.?);
    }
    try warmup(alloc, config, progress);
    if (node != null) node.?.activate();
    for (times) |_, i| {
        if (node != null) {
            node.?.completeOne();
            var cmdstr = try std.mem.join(alloc, " ", config.cmd);
            defer alloc.free(cmdstr);
            node.?.context.log("calling: \"{s}\"\n", .{cmdstr});
        }
        const result = try call(alloc, config.cmd, config.env, &timer);
        defer result.free(alloc);
        processPrint(config, out, "\n{s}\n", .{result.exec.stdout});
        if (result.exec.term.Exited != 0)
            return error.ChildFailed;
        times[i] = result.time;
        if (internalTimes) |it| {
            it[i] = try internalTimerParse(result.exec.stdout, config.internalTimerFmt.?);
            if (config.individualTimes) {
                processPrint(config, out, "internalTime: {s}, ", .{fmtTime(alloc, it[i])});
            }
        }
        if (internalFloats) |it| {
            it[i] = try floatParse(result.exec.stdout, config.floatFmt.?);
            if (config.individualTimes) {
                processPrint(config, out, "internalFloat: {}, ", .{it[i]});
            }
        }
        if (config.individualTimes) {
            processPrint(config, out, "time: {s}\n", .{fmtTime(alloc, result.time)});
        }
    }
    return resultTimes{
        .avg = average(times),
        .stdDev = stdDev(times),
        .internalAvg = if (internalTimes) |it| average(it) else null,
        .internalStdDev = if (internalTimes) |it| stdDev(it) else null,
        .internalFloatAvg = if (internalFloats) |it| averageFloat(it) else null,
        .internalFloatStdDev = if (internalFloats) |it| stdDevFloat(it) else null,
        .iterations = config.iterations,
    };
}

pub fn fmtTime(alloc: mem.Allocator, time: u64) ![]const u8 {
    if (time > std.time.ns_per_s) {
        return std.fmt.allocPrint(alloc, "{d}.{d:0>9} s", .{ time / std.time.ns_per_s, time % std.time.ns_per_s });
    } else if (time > std.time.ns_per_ms) {
        return std.fmt.allocPrint(alloc, "{d}.{d:0>6} ms", .{ time / std.time.ns_per_ms, time % std.time.ns_per_ms });
    } else if (time > std.time.ns_per_us) {
        return std.fmt.allocPrint(alloc, "{d}.{d:0>3} us", .{ time / std.time.ns_per_us, time % std.time.ns_per_us });
    } else {
        return std.fmt.allocPrint(alloc, "{d} ns", .{time});
    }
}

test "call-env" {
    const cmd = [_][]const u8{ "env" };
    const env = [_][]const u8{ "TESTVAR=hello", "SECONDVAR=bye" };
    const result = try call(std.testing.allocator, &cmd, &env, null);
    defer result.free(std.testing.allocator);
    try expect(result.exec.term.Exited == 0);

    
    var splitIter = mem.split(u8, result.exec.stdout, "\n");
    for (env) |_, i|  {
        const out = splitIter.next();
        const in = env[env.len-1-i];
        try expect(out != null);
        //std.debug.print("->\"{s}\" == \"{s}\"\n", .{out.?, in});
        try expect(mem.eql(u8, out.?, in));
    }
    try expect(mem.eql(u8, result.exec.stderr, ""));
    try expect(result.time > 0);
    print("{} env vars matched\n", .{env.len});
}

test "call-echo" {
    const cmd = [_][]const u8{ "echo", "hello-world!" };
    const result = try call(std.testing.allocator, &cmd, null, null);
    defer result.free(std.testing.allocator);

    const timeStr = try fmtTime(std.testing.allocator, result.time);
    defer std.testing.allocator.free(timeStr);
    print("time: {s}\n", .{timeStr});

    try expect(result.exec.term.Exited == 0);
    try expect(mem.eql(u8, result.exec.stdout, "hello-world!\n"));
    try expect(mem.eql(u8, result.exec.stderr, ""));
    try expect(result.time > 0);
}

test "call-sleep" {
    const us_uncert = 5;
    const cmd = [_][]const u8{ "sleep", ".1" };
    const result = try call(std.testing.allocator, &cmd, null, null);
    defer result.free(std.testing.allocator);

    const timeStr = try fmtTime(std.testing.allocator, result.time);
    defer std.testing.allocator.free(timeStr);
    print("time: {s}\n", .{timeStr});

    try expect(result.exec.term.Exited == 0);
    try expect(mem.eql(u8, result.exec.stdout, ""));
    try expect(mem.eql(u8, result.exec.stderr, ""));
    try expect(result.time > (100 - us_uncert) * std.time.ns_per_ms);
    try expect(result.time < (100 + us_uncert) * std.time.ns_per_ms);
}

test "loop-10" {
    const config = callConfig{
        .cmd = &[_][]const u8{ "echo", "test" },
        .iterations = 10,
        .quite = true,
    };
    _ = try callLoop(std.testing.allocator, config, null);
}

test "loop-10-internalTimer" {
    const config = callConfig{
        .cmd = &[_][]const u8{ "echo", "test: 123" },
        .iterations = 10,
        .internalTimerFmt = "test:",
        .quite = true,
    };
    const res = try callLoop(std.testing.allocator, config, null);
    try expect(res.internalAvg.? == 0);
    try expect(res.internalStdDev.? == 0);
    try expect(res.iterations == config.iterations);
}

test "loop-10-internalTimer2" {
    const config = callConfig{
        .cmd = &[_][]const u8{ "echo", "test: 123" },
        .iterations = 10,
        .internalTimerFmt = "test: {s}",
        .quite = true,
    };
    const res = try callLoop(std.testing.allocator, config, null);
    try expect(res.internalAvg.? == 123 * std.time.ns_per_s);
    try expect(res.internalStdDev.? == 0);
    try expect(res.iterations == config.iterations);
}

test "loop-10-internalTimer3" {
    const config = callConfig{
        .cmd = &[_][]const u8{ "echo", "test: 1233.4566.7899 sec" },
        .iterations = 10,
        .internalTimerFmt = "test: {s}.{m}.{u} sec",
        .quite = true,
    };
    const res = try callLoop(std.testing.allocator, config, null);
    try expect(res.internalAvg.? == 1233 * std.time.ns_per_s + 4566 * std.time.ns_per_ms + 7899 * std.time.ns_per_us);
    try expect(res.internalStdDev.? == 0);
    try expect(res.iterations == config.iterations);
}
